<center>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assest/imagenes/sol1.png" alt="NOCARGADO">
      <div class="caption">
        <h3>PUESTA DE SOL</h3>
        <p>Esta foto fue tomada en el mes de abril</p>
        <p><a href="<?php echo base_url(); ?>/assest/imagenes/sol1.png" class="btn btn-primary" role="button">VER</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assest/imagenes/sol2.png" alt="NOCARGADO">
      <div class="caption">
        <h3>PUESTA DE SOL</h3>
        <p>Esta foto fue tomada en el mes de marzo</p>
        <p><a href="<?php echo base_url(); ?>/assest/imagenes/sol2.png" class="btn btn-primary" role="button">VER</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assest/imagenes/sol3.png" alt="NOCARGADO">
      <div class="caption">
        <h3>PUESTA DE SOL</h3>
        <p>Esta foto fue tomada en el mes de septiembre</p>
        <p><a href="<?php echo base_url(); ?>/assest/imagenes/sol3.png" class="btn btn-primary" role="button">VER</a></p>
      </div>
    </div>
  </div>
</div>
</center>


<center>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assest/imagenes/sol4.png" alt="NOCARGADO">
      <div class="caption">
        <h3>PUESTA DE SOL</h3>
        <p>Esta foto fue tomada en el mes de mayo</p>
        <p><a href="<?php echo base_url(); ?>/assest/imagenes/sol4.png" class="btn btn-primary" role="button">VER</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assest/imagenes/sol5.png" alt="NOCARGADO">
      <div class="caption">
        <h3>PUESTA DE SOL</h3>
        <p>Esta foto fue tomada en el mes de enero</p>
        <p><a href="<?php echo base_url(); ?>/assest/imagenes/sol5.png" class="btn btn-primary" role="button">VER</a></p>
      </div>
    </div>
</div>
<div class="col-sm-6 col-md-4">
  <div class="thumbnail">
    <img src="<?php echo base_url(); ?>/assest/imagenes/sol6.png" alt="NOCARGADO">
    <div class="caption">
      <h3>PUESTA DE SOL</h3>
      <p>Esta foto fue tomada en el mes de junio</p>
      <p><a href="<?php echo base_url(); ?>/assest/imagenes/sol6.png" class="btn btn-primary" role="button">VER</a></p>
    </div>
  </div>
</div>
</div>
</center>
